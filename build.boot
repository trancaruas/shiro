(def project 'shiro)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"resources" "src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "RELEASE"]
                            [adzerk/boot-test "RELEASE" :scope "test"]
                            [adzerk/boot-reload        "RELEASE"     :scope "test"]
                            [samestep/boot-refresh "0.1.0"]
                            [org.clojars.scsibug/feedparser-clj "0.4.0"]
                            [morse "0.2.1"]
                            [compojure "1.6.0-beta1"]
                            [pandeiro/boot-http "0.7.3"]
                            [ring "1.6.0-beta6"]
                            [clj-http "3.3.0"]
                            [org.clojure/data.json "0.2.6"]
                            [clj-time "0.12.0"]
                            [ring/ring-jetty-adapter "1.6.0-beta6"]
                            [com.taoensso/timbre "4.8.0-alpha1"]
                            [zenedu.squest/questdb "0.2.2"]])

(require '[adzerk.boot-test :refer [test]]
         '[adzerk.boot-reload    :refer [reload]]
         '[pandeiro.boot-http :refer :all]
         '[samestep.boot-refresh :refer [refresh]])

(task-options!
 aot {:namespace   #{'shiro.core}}
 pom {:project     project
      :version     version
      :description "FIXME: write description"
      :url         "http://example/FIXME"
      :scm         {:url "https://github.com/yourname/shiro"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}}
 jar {:main        'shiro.core
      :file        (str "shiro-" version "-standalone.jar")})

(swap! boot.repl/*default-dependencies* conj
       '[cider/cider-nrepl "0.15.0-SNAPSHOT"]
       '[samestep/boot-refresh "0.1.0" :scope "test"]
       '[refactor-nrepl "2.3.0-SNAPSHOT"])

(swap! boot.repl/*default-middleware* conj
       '[cider.nrepl/cider-middleware]
       '[refactor-nrepl.middleware/wrap-refactor])


(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot) (pom) (uber) (jar) (target :dir dir))))

(deftask run
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (require '[shiro.core :as app])
  (apply (resolve 'app/-main) args))

(deftask dev []
  (comp 
        (speak)
        ;; (watch)
        ;; (refresh)
        ;; (serve)
        (repl)
        ;;(serve)
        ;;(cljs-repl :ids #{"renderer"})
        ;;(reload    :ids #{"renderer"}
        ;;           :on-jsload 'app.renderer/init)
        
        ;;(cljs      :ids #{"renderer"})
        
        ;;(cljs      :ids #{"main"}
        ;;                   :compiler-options {:asset-path "target/main.out"
        ;; :closure-defines {'app.main/dev? true}
        ;; :parallel-build true}))
        ))

