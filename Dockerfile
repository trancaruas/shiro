FROM nginx
COPY nginx-https-proxy.conf /etc/nginx/conf.d/nginx-https-proxy.conf
COPY certs/shiro4_ssl.crt /etc/nginx/ssl/shiro4_ssl.crt 
COPY certs/shiro4_ssl.key.nopasswd /etc/nginx/ssl/shiro4_ssl.key.nopasswd
