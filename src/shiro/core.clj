(ns shiro.core
  (:gen-class)
  (:require [shiro.db :as db]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [morse.api :as api]
            [morse.handlers :refer :all]))

(require '[taoensso.timbre :as log])
(require '[clj-http.client :as http])
(require '[clojure.data.json :as json])
;; consider https://funcool.github.io/cuerdas/latest/
(require '[clojure.string :refer [split]])
(require '[ring.middleware.params :refer [wrap-params]])
(require '[ring.middleware.keyword-params :refer [wrap-keyword-params]])
(require '[ring.middleware.cookies :refer [wrap-cookies]])


;; * CONFIG
(log/merge-config! {:level :debug})
(def config (read-string (slurp ".config.edn")))
(def api-key (:api-key config))
;; (def destiny-key "a45ce642486f4b1a80b7879ae103b26e")

(def trancaruas-id (-> conf :id :trancaruas))
;; (def trancaruas-id "4611686018434864620")

;; (client/get "https://www.bungie.net/Platform/Destiny/2/Stats/GetMembershipIdByDisplayName/trancaruas/" {:headers {:X-API-Key "a45ce642486f4b1a80b7879ae103b26e"}})
;; (-> member :body json/read-json :Response)
;; 4611686018434864620

;; (http/get "https://www.bungie.net/Platform/Destiny/2/Account/4611686018434864620/Summary/" {:headers {:X-API-Key "a45ce642486f4b1a80b7879ae103b26e"}})
;; /{membershipType}/Stats/GetMembershipIdByDisplayName/{displayName}/
;; /2/Stats/GetMembershipIdByDisplayName/trancaruas/

;; https://www.bungie.net/Platform/User/GetBungieAccount/{membershipId}/{membershipType}/
;; (def destiny-char (http/get "https://www.bungie.net/Platform/User/GetBungieAccount/4611686018434864620/2" {:headers {:X-API-Key "a45ce642486f4b1a80b7879ae103b26e"}}))
;; (-> destiny-char :body json/read-json  :Response :bungieNetUser :statusText)
;; "Up&amp;running"

(def token (:token config))
;; (def token "266802299:AAECZfOMx_RiqsJIMPotPLoK3It07wyyu-Y")
;; https://api.telegram.org/bot266802299:AAECZfOMx_RiqsJIMPotPLoK3It07wyyu-Y/getMe
(api/set-webhook token "https://shiro4.sith.su:8443/handler")

;; {:status 200,
;;  :headers
;;  {"Server" "nginx/1.10.0",
;;   "Date" "Mon, 17 Oct 2016 14:24:19 GMT",
;;   "Content-Type" "application/json",
;;   "Content-Length" "57",
;;   "Connection" "close",
;;   "Access-Control-Allow-Origin" "*",
;;   "Access-Control-Allow-Methods" "GET, POST, OPTIONS",
;;   "Access-Control-Expose-Headers"
;;   "Content-Length,Content-Type,Date,Server,Connection",
;;   "Strict-Transport-Security" "max-age=31536000; includeSubdomains"},
;;  :body {:ok true, :result true, :description "Webhook was set"},
;;  :request-time 656,
;;  :trace-redirects
;;  ["https://api.telegram.org/bot266802299:AAECZfOMx_RiqsJIMPotPLoK3It07wyyu-Y/setWebhook"],
;;  :orig-content-encoding nil}

;; PSN API
;; https://github.com/jhewt/gumer-psn/blob/master/psn.js

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!!!"))

(defn status []
  (let [url (str api/base-url token "/getWebhookInfo")
        response (client/get url)]
   (-> response :body json/read-json)))

;; * COMMANDS
;; DONE TODO handle edited_message
;; 16-10-19 23:31:52 kamino INFO [shiro.core:95] - ROUTE:  {:update_id 16152342, :edited_message {:message_id 98, :from {:id 62061037, :first_name "Vadim", :last_name "Shamray", :username "senweitai"}, :chat {:id -171753373, :title "Tower Snadbox", :type "group", :all_members_are_administrators false}, :date 1476919452, :edit_date 1476919549, :text "@gamebot lumberjack", :entities [{:type "mention", :offset 0, :length 8}]}}

(defmacro edited-message
  "Generate command handler"
  [bindings & body]
  (compile-handler :edited_message bindings body))

(defn tokenize [text] (rest (split text #"\s")))

;; (client/get "https://www.bungie.net/Platform/Destiny/2/Stats/GetMembershipIdByDisplayName/trancaruas/" {:headers {:X-API-Key "a45ce642486f4b1a80b7879ae103b26e"}})
;; (-> member :body json/read-json :Response)
;; 4611686018434864620
(def bungie-platform-api "https://www.bungie.net/Platform/Destiny/")
(def membership-req "2/Stats/GetMembershipIdByDisplayName/")
(defn bungie-id [username]
  (let [response (client/get (str bungie-platform-api membership-req username)
                             {:headers {:X-API-Key "a45ce642486f4b1a80b7879ae103b26e"}
                              :socket-timeout 5000 :conn-timeout 10000})
        buid (-> response :body json/read-json :Response)]
    (if (not (= "0" buid)) buid)))
  
;; TODO async req to bungie api
(defn link-psn [tid pid]
  (if-let [bid (bungie-id pid)]
    (do (log/info (str "Linking " pid " with bid " bid))
        (db/update-user tid :pid pid :bid bid))
    (do (log/info (str "Bungie ID for PSN " pid " not found"))
        nil)))


(defhandler bot-api
  (command "help" {{id :id} :chat}
           (log/debug "HELP id:" id)
           (api/send-text token id "Help is on the way"))
  
  (command "psn" {{tid :username} :from text :text {id :id} :chat}
           (let [tokens (tokenize text)
                 pid (first tokens)]
             (if (= 1 (count tokens))
               (do (log/debug "PSN telegram" tid "ID" id "PID" pid)
                   (if (link-psn tid pid)
                     (api/send-text token id (str "PSN " pid " linked to @" tid))
                     (api/send-text token id (str "PSN " pid " not found"))))
               (api/send-text token id "Usage: /psn <psn-id>"))))
  
  (command "lfg" {{user :username} :from} (log/info "LFG" user) "")
  (command "lfm" {{user :username} :from} (log/info "LFM" user) "")
  (message message (log/debug "MESSAGE" message) "")
  (edited-message message (log/debug "EMESSAGE" message) "")
  (inline {user :from q :query} (log/debug "QUERY" q "from:" user) ""))


;; * EXAMPLES
;; "LOG1: " {:update_id 16152321, :message {:message_id 80, :from {:id 62061037, :first_name "Vadim", :last_name "Shamray", :username "senweitai"}, :chat {:id 62061037, :first_name "Vadim", :last_name "Shamray", :username "senweitai", :type "private"}, :date 1476913206, :text "/aegl test1", :entities [{:type "bot_command", :offset 0, :length 5}]}}
;; User senweitai aegled

;; "LOG1: " {:update_id 16152322, :message {:message_id 81, :from {:id 62061037, :first_name "Vadim", :last_name "Shamray", :username "senweitai"}, :chat {:id 62061037, :first_name "Vadim", :last_name "Shamray", :username "senweitai", :type "private"}, :date 1476913231, :text "/help", :entities [{:type "bot_command", :offset 0, :length 5}]}}
;; "HELP: id: " 62061037

;; "LOG: {:update_id 16152328, :message {:message_id 88, :from {:id 62061037, :first_name \"Vadim\", :last_name \"Shamray\", :username \"senweitai\"}, :chat {:id -171753373, :title \"Tower Snadbox\", :type \"group\", :all_members_are_administrators true}, :date 1476913810, :text \"yet another test\"}}"
;; Intercepted message: {:message_id 88, :from {:id 62061037, :first_name Vadim, :last_name Shamray, :username senweitai}, :chat {:id -171753373, :title Tower Snadbox, :type group, :all_members_are_administrators true}, :date 1476913810, :text yet another test}

;; /help List available commands
;; /psn Link your telegram user to PSN
;; /list List current lfg/lfm 
;; /join Join a fireteam from the list
;; /cancel Cancel joining fireteam
;; /lfg Looking for group (if you look for a fireteam)
;; /lfm Looking for member (if you're fireteam looking to fill some positions)
;; /raid Set up a raid party

;; Planned activities:
;; 2: dozniak (@berkus), TheremHarth (@mbravorus), trancaruas (@senweitai) going to Vanguard Archon's Forge at 10/18/16 11:00 PM
;; Enter /join 2 to join this group.
;; 3: dozniak (@berkus) going to Vanguard Archon's Forge at 10/19/16 12:05 AM
;; Enter /join 3 to join this group.
;; 4: dozniak (@berkus) going to Crucible any at 10/19/16 1:17 AM
;; Enter /join 4 to join this group.
;; 5: dozniak (@berkus) going to Crota's End hard at 10/20/16 8:03 PM
;; Enter /join 5 to join this group.

;; Berkus Decker, [20 Oct 2016, 21:53 ]: 
;; /lfg pvp now
;; AEGL Bot, [20 Oct 2016, 21:53 ]: 
;; dozniak (@berkus) is looking for Crucible any group 10/20/16 9:53 PM
;; Enter /join 6 to join this group.
;; control rumble osiris banner

;; help - Get help
;; psn - Link your telegram user to PSN id
;; aegl - Some generic command
;; raid - Join raid schedule

;; https://www.bungie.net/platform/destiny/help/
;; http://destinyapiguide.azurewebsites.net/moreRequests
;; http://pastebin.com/GTrVtScf

;; * ROUTES
(def modes {:control "Crucible Control"})

(defroutes app-routes
  (GET "/" [] "<h1>Hello World</h1>")
  (GET "/handler" [] "<h1>Handler placeholder</h1>")
  ;; (POST "/handler" {{updates :result} :body} (do (prn "update received: " updates) (map bot-api updates)))
  (POST "/handler" {result :body} (let [update (-> result json/read-json)]
                                     (log/debug "ROUTE: " update)
                                     (bot-api update)))
  (route/not-found "<h1>Page not found</h1>"))

(defn wrap-body-string [handler]
  (fn [request]
    (let [body-str (ring.util.request/body-string request)]
      (handler (assoc request :body body-str)))))

(def app (-> app-routes wrap-cookies wrap-keyword-params wrap-params wrap-body-string))
(def server (ring.adapter.jetty/run-jetty app {:port 3300 :join? false}))
(.stop server)
(.start server)

;; * FEED
;; (require '[feedparser-clj.core])
;; (def f (parse-feed "https://www.bungie.net/en/rss/News?currentpage=1"))
;; (count (:entries f))
;; 10
;; shiro.core> (first (:entries f))
;; {:authors (), :categories (), :contents (), :contributors (), :description {:type "text/html", :value "Creating toys for big kids."}, :enclosures (), :link "http://www.bungie.net/en/News/Article/45346", :published-date #inst "2016-10-14T23:28:16.000-00:00", :title "Community Focus - Impact Props", :updated-date nil, :url nil, :uri "BungieNet_ContentItem_45346"}

;; http://masnun.com/2016/03/29/parsing-upwork-job-feed-to-monitor-clojure-jobs.html

;; cljr-add-project-dependency

;; InputFile
;; This object represents the contents of a file to be uploaded. Must
;; be posted using multipart/form-data in the usual way that files are
;; uploaded via the browser.
;; (def shiro-cert (clojure.java.io/file "certs/shiro4_ssl.crt"))

;; (defn set-webhook-shiro
;;   "Register WebHook to receive updates from chats"
;;   [token webhook-url]
;;   (let [url   (str base-url token "/setWebhook")
;;         query {:url webhook-url}
;;         cert  {:certificate shiro-cert}]
;;     (http/post url {:as :json :query-params query
;;                     :multipart [{:name "certificate" :content (clojure.java.io/file "certs/shiro4_ssl.crt")}]})))
