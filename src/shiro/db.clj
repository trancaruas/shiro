(ns shiro.db
  (:require [questdb.core :refer :all]
            [taoensso.timbre :as log]))

(defn schedule []
  {:id 1 :raid "kf" :time "10:00" :list {"trancaruas" "senweitai"}})

;; * DB
;; (def db "db/sched")
;; (create! db)

;; * ADD
;; (put-doc! db {:user "trancaruas" :telegram "senweitai"})
;; ;; => {:user "trancaruas", :telegram "senweitai", :uuid "f213093d-4731-43d2-9951-7a3ac8c06890"}
;; (put-doc! db {:user "berkus" :telegram "dozniak"})
;; ;; => {:user "berkus", :telegram "dozniak", :uuid "f2660e83-b121-4b98-84e0-5bf7c4e5df47"}

;; (get-docs db)
;; ({:user "trancaruas", :telegram "senweitai", :uuid "f213093d-4731-43d2-9951-7a3ac8c06890"}
;;  {:user "berkus", :telegram "dozniak", :uuid "f2660e83-b121-4b98-84e0-5bf7c4e5df47"})

;; (find-doc db {:telegram "senweitai"})
;; {:user "trancaruas", :telegram "senweitai", :uuid "f213093d-4731-43d2-9951-7a3ac8c06890"}

;; * UPDATE
;; (put-doc! db {:user "trancaruas", :telegram "senweitai" :bundie-id "4611686018434864620" :uuid "f213093d-4731-43d2-9951-7a3ac8c06890"})
;; {:user "trancaruas",
;;  :telegram "senweitai",
;;  :uuid "f213093d-4731-43d2-9951-7a3ac8c06890",
;;  :bundie-id "4611686018434864620"}
;; (find-doc db {:telegram "senweitai"})
;; {:user "trancaruas",
;;  :telegram "senweitai",
;;  :uuid "f213093d-4731-43d2-9951-7a3ac8c06890",
;;  :bundie-id "4611686018434864620"}

(defn update-user [tid & {:keys [pid bid]}]
  (let [doc (find-doc db {:tid tid})
        pid (or pid (:pid doc))
        bid (or bid (:bid doc))]
    (put-doc! db (assoc doc :tid tid
                        :pid pid
                        :bid bid))))

;; * GROUPS
;; (put-doc! db {:raid :kf :list {"senweitai" "berkus"} :time "10:00"})
;; {:raid :kf, :list {"senweitai" "berkus"}, :time "10:00", :uuid "bcbae4a8-1225-40a1-8b42-65df9973c17e"}
;; (find-doc db {:raid :kf})
;; {:raid :kf, :list {"senweitai" "berkus"}, :time "10:00", :uuid "bcbae4a8-1225-40a1-8b42-65df9973c17e"}
